#!/usr/bin/env bash

#
#PHP1=$(php ./app/lib/RegistrationHandler.php)
#PHP2=$(php -S 0.0.0.0:80 -t public .htrouter.php)

# Start the first process
php ./app/lib/RegistrationHandler.php &
status=$?
if [ $status -ne 0 ]; then
  echo "Failed to start my_first_process: $status"
  exit $status
fi

# Start the second process
php -S 0.0.0.0:8000 -t public .htrouter.php &
status=$?

if [ $status -ne 0 ]; then
  echo "Failed to start my_second_process: $status"
  exit $status
fi

# Naive check runs checks once a minute to see if either of the processes exited.
# This illustrates part of the heavy lifting you need to do if you want to run
# more than one service in a container. The container exits with an error
# if it detects that either of the processes has exited.
# Otherwise it loops forever, waking up every 60 seconds

while true; do
  sleep 60
done