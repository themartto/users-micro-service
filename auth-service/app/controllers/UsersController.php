<?php

use Phalcon\Http\Response;

class UsersController extends ControllerBase
{
	public function registerAction() {

		$request = $this->request->getJsonRawBody();
		$response = new Response();

		$username = $request->username;
		$password = $request->password;
		$confirmPassword = $request->confirmPassword;
		$clientHash = $request->clientHash;

		if($this->redis->exists($username)) {
			throw new Exception('{error:UsernameAlreadyExistsException}');
		}

		if($password !== $confirmPassword) {
			throw new Exception('{error:PasswordMismatchException}');
		}

		$password = $this->security->hash($password);

		$message = [
            'task' => [
                'job' => 'registerUser',
                'status' => 'awaiting',
            ],
            'payload' => [
                'username' => $username,
                'password' => $password,
                'clientHash' => $clientHash
            ]
        ];

		//send register user action
		$this->rabbitMq->send(json_encode($message), 'tasks');

		$response->setStatusCode(200, 'OK');
		$response->setContent('{status: WaitingConfirmation}');
		$response->send();
	}
}

