<?php

use Phalcon\Http\Response;

class IndexController extends ControllerBase
{

    public function indexAction()
    {
        $response = new Response();

        $response->setStatusCode(404, 'Not Found');
        $response->setContent("Watcha doin' here monkey!?");
        $response->send();
    }

}

