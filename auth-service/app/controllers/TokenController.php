<?php

use Nowakowskir\JWT\JWT;
use Nowakowskir\JWT\TokenEncoded;
use Nowakowskir\JWT\TokenDecoded;

class TokenController extends ControllerBase
{

	private $tokenEncoded;

    public function initialize() {
		parent::initialize();
    }

    public function authAction() {

    	$request = $this->request->getJsonRawBody();

    	$username = $request->username;
    	$password = $request->password;

    	if($this->redis->exists($username)) {

    		$userFromCache = $this->redis->get($username);

    		$tokenEncoded = new TokenEncoded($userFromCache['token']);

			try {
				$tokenEncoded->validate($username);
			} catch (IntegrityViolationException $e) {
				// Token is not trusted
                throw new Exception($e->getMessage());
			} catch(TokenExpiredException $e) {
				// Token expired (exp date reached)
                if ($this->security->checkHash($password, $userFromCache['password'])) {
                    $tokenDecoded = new TokenDecoded([], ['iat' => time() + 24 * 60 * 60]);
                    $tokenDecoded->encrypt($username);

                    $this->redis->save($username, [
                        'password' => $password,
                        'token' => $tokenDecoded->__toString()
                    ]);
                }
			} catch(TokenInactiveException $e) {
				// Token is not yet active (nbf date not reached)
			} catch(Exception $e) {
				// Something else gone wrong
                throw new Exception($e->getMessage());
			}

			if (!$this->security->checkHash($password, $userFromCache['password'])) {
                throw new Exception("invalid username/password");
            }

            $this->response->setStatusCode(200, 'OK');
            $this->response->setContent(json_encode(['token' => $userFromCache['token']]));
            $this->response->send();

    	} else {
            throw new Exception("{error: NO SUCH USER!}");
        }



    }

}

