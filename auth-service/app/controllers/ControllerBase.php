<?php

use App\lib\RabbitMq;
use Phalcon\Mvc\Controller;
use Phalcon\Security;
use Phalcon\Cache\Backend\Redis as Redis;
use Phalcon\Cache\Frontend\Data as UsersCache;

class ControllerBase extends Controller {

    const REDIS_CONFIG = [
        "host"       => "redis",
        "port"       => 6379,
        "persistent" => false,
        "index"      => 0,
    ];

	/**
	 * Le Cache
	 * @var $redis
	 */
	protected $redis;

	/**
	 * @var RabbitMq
	 */
	protected $rabbitMq;

	protected $security;

	public function initialize() {

		$this->security = new Security();

		$this->rabbitMq = new RabbitMq();

		// Create the Cache setting redis connection options
		$this->redis = new Redis(new UsersCache(), self::REDIS_CONFIG);

        $this->view->disable();
	}

}
