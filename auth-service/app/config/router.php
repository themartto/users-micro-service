<?php

$router = $di->getRouter();

// Define your routes here

$router->add('/index', ['controller' => 'index', 'action' => 'index',]);

$router->add('/token', ['controller' => 'token', 'action' => 'index',]);

$router->add('/token/auth', ['controller' => 'token', 'action' => 'auth',]);

$router->addPost('/users/register', ['controller' => 'users', 'action' => 'register',]);

$router->handle();
