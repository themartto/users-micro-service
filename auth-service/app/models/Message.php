<?php

class Message extends Model {

    public $task;

    public $payload;

    public function __construct($task, $payload) {
        $this->task = $task;
        $this->payload = $payload;
    }

    public function getTask() {
        return $this->task;
    }

    public function getPayload() {
        return $this->payload;
    }
}