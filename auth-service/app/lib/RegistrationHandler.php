<?php
namespace App\lib;

//as this will be run separately as process
require __DIR__ . '/../../vendor/autoload.php';

use App\lib\RabbitMq;
use Nowakowskir\JWT\TokenDecoded;
use Phalcon\Cache\Backend\Redis as Redis;
use Phalcon\Cache\Frontend\Data as UsersCache;

class RegistrationHandler {

	const REDIS_CONFIG = [
		"host"       => "redis",
		"port"       => 6379,
		"persistent" => false,
		"index"      => 0,
	];

	const REDIS_CACHE_LIFETIME = 172800;

	public static function start() {
		self::listenForRegistrations();
	}

	private static function listenForRegistrations(){

		$jwt = new TokenDecoded([], ['iat' => time() + 24 * 60 * 60]);

		$redis = new Redis(new UsersCache(['lifetime' => self::REDIS_CACHE_LIFETIME]), self::REDIS_CONFIG);

		$callback = function ($msg) use ($jwt, $redis) {
			if(!self::isJson($msg->body)) {
				echo "invalid payload";
			}

			$data = json_decode($msg->body);

			if($data->task->job == 'userRegistration' && $data->task->status == 'success') {
				$token = $jwt->encode($data->payload->username);

				$redis->save($data->payload->username, [
					'password' => $data->payload->password,
					'token' => $token->__toString(),
				]);
			}
            var_dump($data->payload->username . "saved in cache");
		};

		RabbitMq::subscribe('registration_handler', $callback);

	}

	private function isJson($string) {
		json_decode($string);
		return (json_last_error() == JSON_ERROR_NONE);
	}

}

RegistrationHandler::start();