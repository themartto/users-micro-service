<?php
namespace App\lib;

use App\interfaces\MessageBroker;
use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;

class RabbitMq implements MessageBroker {

	//TODO implement docker envs for those
	const RABBITMQ_HOST = 'rabbitmq';
	const RABBITMQ_PORT = 5672;
	const RABBITMQ_USER = 'guest';
	const RABBITMQ_PASS = 'guest';

	public function __construct() {

	}

    public static function connect() {
        while (true) {
            try {
                return self::connection();
            } catch (\Exception $e) {
                fwrite(STDOUT, $e->getMessage() . "\n");
                sleep(2);
            }
        }
    }

	static public function connection($host = self::RABBITMQ_HOST, $port = self::RABBITMQ_PORT,
                                      $user = self::RABBITMQ_USER, $password = self::RABBITMQ_PASS) {
        return new AMQPStreamConnection($host, $port, $user, $password);
    }

    /**
     * @param string $message
     * @param string $channel
     * @throws \Exception
     */
	public static function send(string $message, string $channel): void {

	    $con = self::connect()->channel();

		$con->exchange_declare($channel, 'fanout', false, false, false);

		$msg = new AMQPMessage($message);

        try{
			$con->basic_publish($msg, $channel, 'task');
        } catch (\Exception $error) {
            throw new \Exception($error);
        }
	}

    /**
     * @param string $channelName
     * @param $callback
     * @throws \ErrorException
     */
    public static function subscribe(string $channel, $callback) {

		$con = self::connect()->channel();

        $con->exchange_declare('info', 'fanout', false, false, false);

		$con->queue_declare($channel, false, false, false, false);

        $con->queue_bind($channel, 'info', 'info');

		$con->basic_consume($channel, '', false, true, false, false, $callback);

		while ($con->is_consuming()) {
			$con->wait();
		}
	}
}