<?php

namespace App\interfaces;

interface MessageBroker {

    /**
     * @param string $message
     * @param string $channel
     */
    public static function send(string $message, string $channel): void;

    /**
     * @param string $channel
     * @param $callback
     * @return mixed
     */
    public static function subscribe(string $channel, $callback);


}