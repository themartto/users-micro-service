<?php

require_once __DIR__ . '/vendor/autoload.php';
use App\app\Workers\RabbitMq;
use App\app\Workers\DatabaseWorker;


class UsersService {

	public static function run() {

		$callback = function ($mqMeg) {
			if(!self::isJson($mqMeg->body)){
				fwrite(STDERR,'Invalid Payload');
			}

			$taskParams = json_decode($mqMeg->body);

			if(method_exists(DatabaseWorker::class, $taskParams->task->job)) {
			    DatabaseWorker::verifyTable();
				DatabaseWorker::handleTask($taskParams->task->job, $taskParams->payload);
			} else {
				fwrite(STDERR, 'Invalid Task');
			}
		};

		RabbitMq::subscribe('tasks', $callback);
	}

	private function isJson($string) {
		json_decode($string);
		return (json_last_error() == JSON_ERROR_NONE);
	}
}

$boom = UsersService::run();
