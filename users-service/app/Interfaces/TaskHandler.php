<?php
namespace App\app\Interfaces;

interface TaskHandler {
    /**
     * @param $payload
     * @return mixed
     */
    public function registerUser($payload);
    public function deleteUser($payload);
    public function updateUser($payload);
    public function getUser($payload);
}