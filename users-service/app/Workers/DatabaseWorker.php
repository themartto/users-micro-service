<?php
namespace App\app\Workers;

use App\app\Interfaces\TaskHandler;
use App\app\Workers\PgSql as PgSql;

class DatabaseWorker implements TaskHandler {

    private $pgSql;

    public function __construct($task, $payload) {
        $this->verifyTable();
    }

    public static function verifyTable() {

        $con = PgSql::get()->connect();

        $query = 'CREATE TABLE IF NOT EXISTS users (
                    id serial PRIMARY KEY,
                    username VARCHAR (50) UNIQUE NOT NULL,
                    password VARCHAR (120) NOT NULL
                    );';
        $stmt = $con->prepare($query);
        $stmt->execute();
    }

    static public function handleTask($task, $payload) {

    	$result = self::{$task}($payload);

        if ($result) {
            var_dump("sending MSG from DATABASE");
            RabbitMq::send($result, 'info');
        }

    }

	/**
	 * @param $payload
	 * @return false|mixed|string
	 * @throws \Exception
	 */
	public function registerUser($payload) {

		$con = PgSql::get()->connect();

        $sql = 'INSERT INTO users (username,password) VALUES(:username,:password) RETURNING (id, username, password)';
        $stmt = $con->prepare($sql);

        // pass values to the statement
        $stmt->bindValue(':username', $payload->username);
        $stmt->bindValue(':password', $payload->password);

        $error = false;

        try {
            $stmt->execute();
        } catch (\Exception $e) {
			$error = true;
            fwrite(STDERR, $e->getMessage());
        }

        $res = $stmt->fetchObject();

        $tmp = self::parseRow((string)$res->row);

        return json_encode([
            'task' => [
            	'job' => 'userRegistration',
				'status' => $error ? 'error' : 'success',
			],
			'payload' => [
            	'id' => $tmp[0],
               	'username' => $tmp[1],
               	'password' => $tmp[2],
				'clientHash' => $payload->clientHash
			]
        ]);
    }

    public function deleteUser($payload) {
        $sql = 'DELETE FROM users WHERE username = :username RETURNING username';
        $stmt = $this->pgSql->prepare($sql);

        $stmt->bindValue(':username', $payload['username']);

        try {
            $stmt->execute();
        } catch (\Exception $e) {
			throw new \Exception($e->getMessage());
        }

        $res = $stmt->fetchAll();

        return json_encode([
        	'task' => [
        		'job' => 'userDelete',
				'status' => 'success'
			]
		]);
    }

    public function updateUser($payload) {

    	$id = $this->getUser($payload, true)[0];
        var_dump($id);
        $sql = 'UPDATE users SET username = :username, password = :password WHERE id = :id RETURNING (username, password)';
        $stmt = $this->pgSql->prepare($sql);
        $stmt->bindValue(':username', $payload['username']);
        $stmt->bindValue(':password', $payload['password']);
        $stmt->bindValue(':id', (int)$id);

        try {
            $stmt->execute();
        } catch (\Exception $e) {
			throw new \Exception($e->getMessage());
        }

        $res = $stmt->fetchObject();

		$row = $this->parseRow($res->row);

		return json_encode([
			'task' => [
				'username' => $row[1],
				'password' => $row[2],
			],
		]);
    }

    public function getUser($payload, $asArray = false)
    {
        $sql = 'SELECT (id, username, password) FROM users WHERE username = :username';
        $stmt = $this->pgSql->prepare($sql);

        $stmt->bindValue(':username', $payload['username']);

        try {
            $stmt->execute();
        } catch (\Exception $e) {
			throw new \Exception($e->getMessage());
        }

        $res = $stmt->fetchObject();

		$row = $this->parseRow($res->row);

		if (!$asArray){
			return json_encode([
				'task' => [
					'notification' => 'userInfo',
					'status' => 'success',
				],
				'payload' => [
					'id' =>$row[0],
					'username' => $row[1],
					'password' => $row[2],
				]
			]);
		} else {
			return $row;
		}
    }

     private function parseRow($row) {
        $arr = explode(',', (string)$row);
        return array_map(function ($el) {
            return trim($el, ' ()');
        }, $arr);
    }
}