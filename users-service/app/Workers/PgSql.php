<?php

namespace App\app\Workers;

class PgSql {
    const PG_HOST = 'postgres';
    const PG_PORT = 5432;
    const PG_DATABASE = 'users_service';
    const PG_USER = 'adminski';
    const PG_PASSWORD = 'nastypass12';
    /**
     * Connection
     * @var type
     */
    private static $conn;

    /**
     * Connect to the database and return an instance of \PDO object
     * @return \PDO
     * @throws \Exception
     */
    public function connect($host = self::PG_HOST, $port = self::PG_PORT, $database = self::PG_DATABASE,
                            $user = self::PG_USER, $password = self::PG_PASSWORD) {

        // connect to the postgresql database
        $conStr = sprintf("pgsql:host=%s;port=%d;dbname=%s;user=%s;password=%s",
            	$host, $port, $database, $user, $password);

        $pdo = new \PDO($conStr);
        $pdo->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);

        return $pdo;
    }

    /**
     * return an instance of the Connection object
     * @return type
     */
    public static function get() {
        if (null === static::$conn) {
            static::$conn = new static();
        }

        return static::$conn;
    }

    protected function __construct() {

    }

    private function __clone() {

    }

    private function __wakeup() {

    }

}