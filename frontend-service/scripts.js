const ws = new WebSocket('ws://localhost:3333');

ws.onopen = function(event) {
	if (typeof localStorage.hash == "undefined") {
		ws.send(JSON.stringify("asd"));

		ws.onmessage = function (msg) {
			let message = JSON.parse(msg.data);
			console.log(message);
			if (typeof message.hash !== "undefined") {
				console.log(localStorage.hash);
				console.log(message.hash);
				if (message.hash !== localStorage.hash) {
					localStorage.hash = message.hash;
				}
			}
		}
	} else {
		ws.send(JSON.stringify({itsme: localStorage.hash}))
	}
	console.log(localStorage.hash);
};

function call(url, method, data) {

    return fetch(url, {
        method: method,
        mode: 'no-cors',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(data)
    });
}