ws.onmessage = function (msg) {
	let message = JSON.parse(msg.data);

	console.log(message);
	let el = document.getElementById("notification");
	el.innerText = message.message;
	el.style.display = '';
	setTimeout(function () {
		el.style.display = 'none';
	}, 30000)
};

document.getElementById('registerButton').onclick = function () {

	let data = {
		username : document.getElementById('username').value,
		password: document.getElementById('password').value,
		confirmPassword: document.getElementById('confirmPassword').value,
		clientHash: localStorage.hash
	};

	call('http://localhost:8000/users/register', 'POST', data)
		.then(function (response) {
			console.log(response);
		}).catch(function (error) {
			console.log(error)
		})
}