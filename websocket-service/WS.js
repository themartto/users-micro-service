#!/usr/bin/env node

const WebSocket = require('ws');

const wss = new WebSocket.Server({
    port: 3333,
    perMessageDeflate: {
        zlibDeflateOptions: {
            // See zlib defaults.
            chunkSize: 1024,
            memLevel: 7,
            level: 3
        },
        zlibInflateOptions: {
            chunkSize: 10 * 1024
        },
        // Other options settable:
        clientNoContextTakeover: true, // Defaults to negotiated value.
        serverNoContextTakeover: true, // Defaults to negotiated value.
        serverMaxWindowBits: 10, // Defaults to negotiated value.
        // Below options specified as default values.
        concurrencyLimit: 10, // Limits zlib concurrency for perf.
        threshold: 1024 // Size (in bytes) below which messages
        // should not be compressed.
    }
});

let clients = {};

wss.on('connection', function connection(ws) {
	ws.on('message', function incoming(msg) {
	    let message = JSON.parse(msg);
        if(typeof message.itsme !== "undefined") { //TODO this should be changed with decode hash method
            clients[message.itsme] = {
                connection: ws
            };
        } else {
            let hash = generateHash(10);
            ws.send(JSON.stringify({hash: hash}));
            console.log("send hash");
        }
	});
	console.log(clients)
});

wss.on('error', (err) => {
    console.log(err.message)
});

const amqp = require('amqplib/callback_api');

setTimeout(function () {
    console.log("connecting to mq");
    amqp.connect('amqp://rabbitmq:5672', function (error0, connection) {
        if (error0) {
            throw error0;
        }
        connection.createChannel(function (error1, channel) {
            if (error1) {
                throw error1;
            }

            var queue = 'websocket';

            channel.assertQueue(queue, {
                durable: false
            });

            channel.bindQueue(queue, 'info', 'info');

            channel.consume(queue, function (msg) {
                console.log(msg);
                let decoded_msg = JSON.parse(msg.content.toString());
                console.log(decoded_msg);
				if (typeof (decoded_msg.payload !== "undefined")) {
					let clientIdentifier = decoded_msg.payload.clientHash;
                    console.log(clientIdentifier);
					if (typeof (clients[clientIdentifier] !== "undefined")) {
						if (decoded_msg.task.status == 'success') {
							clients[clientIdentifier].connection.send(JSON.stringify({message: decoded_msg.task.status}));
						}
					}
				}

            }, {
                noAck: true
            });
        });
    });

}, 15000);


const crypto = require("crypto");

function generateHash(length) {
	return crypto.randomBytes(length).toString('hex');
}